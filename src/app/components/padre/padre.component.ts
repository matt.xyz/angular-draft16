import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padreMensaje = 'mensaje del padre. Cambio';
  secondMesg = 'Segundo Mensaje';
  TercerMsg = 'Este es el tercer mensaje';
  CuartoMsg = 'Cuarto Mensaje';
  QuintoMsg = 'Cambio y Fuera.'
  
  constructor() { }

  ngOnInit(): void {
  }

}
